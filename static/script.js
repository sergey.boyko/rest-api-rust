const url = '/files/upload';
let form = document.forms.namedItem("files_form");

function addElement(parentId, elementTag, elementId, html) {
    // Adds an element to the document
    var p = document.getElementById(parentId);
    var newElement = document.createElement(elementTag);
    newElement.setAttribute('id', elementId);
    newElement.innerHTML = html;
    p.appendChild(newElement);
}

function removeElement(elementId) {
    // Removes an element from the document
    var element = document.getElementById(elementId);
    element.parentNode.removeChild(element);
}

function handleImagePreview(dx, dy, image) {
    let canvas = document.getElementById("previews");
    let ctx = canvas.getContext("2d");

    let c_image = new Image();
    c_image.src = "data:image/png;base64," + image.data;
    c_image.onload = function () {
        ctx.drawImage(c_image, dx, dy);
    };
}

function handleJsonResult(result) {
    let canvas = document.getElementById("previews");
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);

    for (i = 0, column = 0, row = 0; i < result.images.length; i++) {
        const image = result.images[i];
        if (image.success == null) {
            if (image.failure != null) {
                console.error("ERROR: " + image.failure);
            }
            continue;
        }

        canvas.height = 100 * (row + 1);
        handleImagePreview(column * 100, row * 100, image.success);

        if (column >= 7) {
            column = 0;
            row++;
        } else {
            column++;
        }
    }
}

function readFileAsync(file) {
    return new Promise((resolve, reject) => {
        let reader = new FileReader();

        reader.onload = () => {
            resolve(reader.result.replace(/^data:.+;base64,/, ''));
        };

        reader.onerror = reject;
        reader.readAsDataURL(file);
    })
}

async function dosubmit() {
    const formData = new FormData();

    const optional_url = document.getElementById('url');
    if (optional_url.value !== "") {
        const url_node = {url: optional_url.value};
        formData.append('images', new Blob([JSON.stringify(url_node)], {type: "application/json"}));
    }

    const elementRawFiles = document.getElementById('rawFiles');
    for (let i = 0; i < elementRawFiles.files.length; i++) {
        formData.append('images', elementRawFiles.files[i])
    }

    const elementBase64Files = document.getElementById('base64Files');
    for (let i = 0; i < elementBase64Files.files.length; i++) {
        const file = elementBase64Files.files[i];
        let encoded = await readFileAsync(file);

        let image = {base64: {filename: file.name, data: encoded}};
        const blob = new Blob([JSON.stringify(image)], {type: "application/json"});
        formData.append('images', blob)
    }

    const response = await fetch(url, {
        method: 'POST',
        body: formData,
    });

    if (response.status === 400) {
        alert("Error: " + await response.text());
        return;
    }

    handleJsonResult(await response.json())
}

use actix_web::{middleware::Logger, App, HttpServer};
use frontend::routes;
use utils::CONFIG;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};


#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    env_logger::init();

    let mut builder =
        SslAcceptor::mozilla_intermediate(SslMethod::tls())
            .unwrap();
    builder
        .set_private_key_file("key.pem", SslFiletype::PEM)
        .unwrap();
    builder.set_certificate_chain_file("cert.pem").unwrap();

    HttpServer::new(|| {
        App::new()
            .wrap(Logger::default())
            .configure(routes)
    })
        .bind_openssl(&CONFIG.server_url, builder)?
        .run()
        .await
}

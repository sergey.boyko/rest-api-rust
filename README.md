# rest-api-rust

`rest-api-rust` is a REST server consists of one service and implements only one method `files/upload`.
The `rest-api-rust` written in pure Rust based on asynchronous `actix-web` and `tokio` libraries,
uses `OpenCV` library as FFI dependence.

## Getting Started

### Configuration parameters

Change the parameters of the `.env` configuration file as you wish.

### Certificates

If you want you can create a [self-]signed certificates in the application root directory.
For example:

```bash
openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'
```

Else the certificates will be created during the `docker-compose up` running.

### Build

Make sure you have already installed both Docker Engine and Docker Compose.

Build the `web-srv` (also known as `rest-api-rust`)

```bash
docker-compose up
```

### Use application

Enter https://127.0.0.1:8800/ in a browser to see the application running.

## Protocol

The application implements only one method `files/upload`.

The method takes multipart/form-data request that may contain one or more parts.
Each part is expected to has the following headers:
`Content-Disposition: form-data; name="images" [; filename="<filename>";]`
where the filename is optional parameter (used only if the `Content-Type: image/*`).

Each part is expected to has one of the following payload structures:

### Raw image data

`filename` is specified within the `Content-Disposition` of the part.

### Base64 encoded file

```json
{
    "base64": {
        "filename": String,
        "data": String
    }
}
```

### URL

Uploading using the specified URL.

```json
{
    "url": String
}
```

Response is either text/plan error description (if the multipart request is invalid),
or the following JSON structure:

```json
{
    "images": [
        {
            "success": [null, { "filename": String, "data": String}],
            "failure": [null, String]
        }
    ]
}
```
where the `data` contains Base64 encoded preview.

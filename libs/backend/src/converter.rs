use super::check_file_extension;
use utils::{CONFIG, WebResult, WebError, Interpolation, IsColor, IplImage, resize};
use std::path::Path;
use tokio::fs::File;
use tokio::prelude::*;


/// ImagePreview is an image converted to Base64
pub struct ImagePreviewBase64(String);

impl ImagePreviewBase64 {
    pub fn into_string(self) -> String {
        self.0
    }
}

/// Load an image from the `src_path`, create a PNG preview with the specified in the `CONFIG` sizes
/// and encode it to Base64.
pub async fn create_png_base64_preview(src_path: String, dst_width: u32, dst_height: u32)
                                       -> WebResult<ImagePreviewBase64> {
    check_file_extension(&src_path)?;

    let preview_filename = gen_preview_filename(&src_path)?;
    let preview_path = format!("{}/{}", CONFIG.work_dir, preview_filename);

    let src = IplImage::from_file(src_path, IsColor::CvLoadImageColor)?;
    // create an image with the same depth and count of channels
    let mut dst =
        IplImage::from_size(dst_width, dst_height, src.depth(), src.channels_count())?;

    resize(&src, &mut dst, Interpolation::CvInterLinear);
    dst.save(preview_path.clone())?;

    let preview_data = load_file(&preview_path).await?;
    let preview_data = base64::encode(&preview_data);

    Ok(ImagePreviewBase64(preview_data))
}

/// Generate an image preview path from the source `filepath`.
/// Note the function returns path to file with extension = png always.
pub fn gen_preview_filename(filepath: &String) -> WebResult<String> {
    let filepath = Path::new(filepath);
    let stem = filepath.file_stem()
        // filename has to be not empty
        .ok_or(WebError::InvalidFilename)?
        .to_str()
        .ok_or(WebError::InvalidFilename)?;
    Ok(format!("{}.preview.png", stem))
}

async fn load_file(filepath: &String) -> WebResult<Vec<u8>> {
    let mut file = File::open(filepath)
        .await
        .map_err(|err| {
            error!("could not open file. Error: {}", &err);
            WebError::InternalError
        })?;

    let mut contents = Vec::new();
    let _len = file.read_to_end(&mut contents)
        .await
        .map_err(|err| {
            error!("could not read from file. Error: {}", &err);
            WebError::InternalError
        })?;
    Ok(contents)
}

use super::{ImagePreviewBase64, create_png_base64_preview, check_filename, find_filename_from_url};
use utils::{WebResult, WebError, CONFIG};
use futures::StreamExt;
use tokio::prelude::*;
use tokio::fs::File;
use openssl::ssl::{SslConnector, SslMethod};

// TODO consider replace the web dependency to some non-backend module
use actix_web::client::Client;
use actix_http::http::StatusCode;
use actix_http::client::Connector;


/// Download file from remote service using URL
/// and create preview encoded to Base64.
pub async fn upload_from_url(url: String)
                             -> WebResult<ImagePreviewBase64> {
    let ssl_conn = SslConnector::builder(SslMethod::tls())
        .map_err(|err| {
            error!("SSL error: {}", err);
            WebError::InternalError
        })?
        .build();
    let connector = Connector::new()
        .ssl(ssl_conn)
        .finish();

    let client = Client::build().connector(connector).finish();

    // the URL will be checked within the Client
    let mut response = client.get(url.clone())
        .header("User-Agent", "Actix-web")
        .send()
        .await
        .map_err(|err| {
            debug!("Remote URL connection error: {}", err);
            WebError::InvalidUrl
        })?;

    if response.status().as_u16() != StatusCode::OK {
        // Now we do not support the REDIRECTION status code.
        debug!("Remote URL connection error: StatusCode = {}", response.status().as_u16());
        return Err(WebError::InvalidUrl);
    }

    // TODO check the image size

    let filename = response.headers()
        .get("content-disposition")
        .ok_or(WebError::InvalidRemoteFile)
        .and_then(|x| x.to_str().map_err(|_err| WebError::InvalidRemoteFile))
        .and_then(|x| find_filename_from_content_disposition(x))
        .or_else(|_err| find_filename_from_url(&url))?;

    // If the file is no an image, the opencv will notice us,
    // check only if the filename does not contain a directories
    check_filename(&filename)?;
    let filename = format!("{}/{}", CONFIG.work_dir, filename);

    // write to the original file
    let mut file: File = File::create(&filename)
        .await
        .map_err(|err| {
            error!("could not open file. Error: {}", &err);
            WebError::InternalError
        })?;

    while let Some(chunk) = response.next().await {
        let data = chunk.map_err(|_err| WebError::InvalidRemoteFile)?;
        file.write_all(&data)
            .await
            .map_err(|err| {
                error!("could not write to file. Error: {}", &err);
                WebError::InternalError
            })?;
        // Flush to be sure the file has been written to the filesystem
        file.flush()
            .await
            .map_err(|err| {
                error!("could not flush. Error: {}", &err);
                WebError::InternalError
            })?;
    }

    Ok(create_png_base64_preview(filename, CONFIG.preview_width, CONFIG.preview_height).await?)
}

pub fn find_filename_from_content_disposition(content_disposition: &str) -> WebResult<String> {
    let key = "filename=\"";
    let pos = content_disposition.find(key).ok_or(WebError::InvalidRemoteFile)?;

    let (_, tmp) = content_disposition.split_at(pos + key.len());
    let pos = tmp.find('"').ok_or(WebError::InvalidRemoteFile)?;

    let (dst, _) = tmp.split_at(pos);

    Ok(dst.to_string())
}

use utils::{WebResult, WebError, CONFIG};
use super::{ImagePreviewBase64, create_png_base64_preview, check_filename};
use tokio::prelude::*;
use tokio::fs::File;


pub struct Base64Image {
    filename: String,
    data: String,
}

impl Base64Image {
    pub fn new(filename: String, data: String) -> Base64Image {
        Base64Image { filename, data }
    }
}

/// Decode and write the Base64 data to file with the specified filename
/// and create preview encoded to Base64.
pub async fn upload_from_base64(image: Base64Image)
                                -> WebResult<ImagePreviewBase64> {
    // If the file is no an image, the opencv will notice us,
    // check only if the filename does not contain a directories
    check_filename(&image.filename)?;

    let filename = format!("{}/{}", CONFIG.work_dir, image.filename);

    let data = base64::decode(&image.data)
        .map_err(|_err| WebError::ImageBase64Deserialize)?;

    // write to the original file
    let mut file: File = File::create(&filename)
        .await
        .map_err(|err| {
            error!("could not open file. Error: {}", &err);
            WebError::InternalError
        })?;

    file.write_all(&data)
        .await
        .map_err(|err| {
            error!("could not write to file. Error: {}", &err);
            WebError::InternalError
        })?;
    // Flush to be sure the file has been written to the filesystem
    file.flush()
        .await
        .map_err(|err| {
            error!("could not flush. Error: {}", &err);
            WebError::InternalError
        })?;

    Ok(create_png_base64_preview(filename, CONFIG.preview_width, CONFIG.preview_height).await?)
}

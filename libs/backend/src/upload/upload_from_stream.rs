use utils::{WebResult, WebError, CONFIG};
use super::{ImagePreviewBase64, create_png_base64_preview, check_filename};
use futures::{Stream, StreamExt};
use tokio::prelude::*;
use tokio::fs::File;
use bytes::Bytes;


pub struct RawImageInfo {
    filename: String,
}

impl RawImageInfo {
    pub fn new(name: String) -> RawImageInfo {
        RawImageInfo { filename: name }
    }
}

/// Write chunk of file data to file with the specified filename
/// and create preview encoded to Base64.
pub async fn upload_from_stream<S>(info: RawImageInfo, mut data_stream: S)
                                   -> WebResult<ImagePreviewBase64>
    where S: Stream<Item=WebResult<Bytes>> + std::marker::Unpin {
    check_filename(&info.filename)?;

    let filename = format!("{}/{}", CONFIG.work_dir, info.filename);

    // write to the original file
    let mut file: File = File::create(&filename)
        .await
        .map_err(|err| {
            error!("could not open file. Error: {}", &err);
            WebError::InternalError
        })?;

    while let Some(chunk) = data_stream.next().await {
        let data = chunk?;
        file.write_all(&data)
            .await
            .map_err(|err| {
                error!("could not write to file. Error: {}", &err);
                WebError::InternalError
            })?;
        // Flush to be sure the file has been written to the filesystem
        file.flush()
            .await
            .map_err(|err| {
                error!("could not flush. Error: {}", &err);
                WebError::InternalError
            })?;
    }

    Ok(create_png_base64_preview(filename, CONFIG.preview_width, CONFIG.preview_height).await?)
}

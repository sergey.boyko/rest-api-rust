#[macro_use]
extern crate log;

mod upload;
mod converter;
mod common;

pub use upload::{RawImageInfo, Base64Image, upload_from_stream, upload_from_url, upload_from_base64};
pub use converter::ImagePreviewBase64;

/// Note do not use the functions except in tests
pub use upload::find_filename_from_content_disposition as _find_filename;
pub use converter::{gen_preview_filename as _gen_preview_filename,
                    create_png_base64_preview as _create_png_base64_preview};
pub use common::{check_file_extension, check_filename, find_filename_from_url};

use converter::create_png_base64_preview;

use std::path::Path;
use utils::{WebResult, WebError};
use url::Url;

/// Check if the filename does not contain '/' or '\' characters.
/// Note do not use the functions except in tests.l
pub fn check_filename(filename: &String) -> WebResult<()> {
    match filename.find(|ch| ch == '/' || ch == '\\') {
        Some(_) => Err(WebError::InvalidFilename),
        _ => Ok(())
    }
}

/// Check if the filename contains an extension.
pub fn check_file_extension(filepath: &str) -> WebResult<()> {
    Path::new(filepath)
        .extension()
        .ok_or(WebError::InvalidFilename)
        .and_then(|ext| match ext.is_empty() {
            // extension cannot be an empty
            true => Err(WebError::InvalidFilename),
            _ => Ok(())
        })
}

pub fn find_filename_from_url(url: &String) -> WebResult<String> {
    let url = Url::parse(url.as_str()).map_err(|err| {
        debug!("URL parse error: {}", err);
        WebError::InvalidUrl
    })?;
    let path = Path::new(url.path());
    let filename = path.file_name().ok_or_else(|| WebError::InvalidUrl)?;
    let filename = filename.to_str().ok_or_else(|| WebError::InvalidUrl)?;
    check_file_extension(filename).map_err(|_err| WebError::InvalidUrl)?;
    Ok(filename.to_string())
}

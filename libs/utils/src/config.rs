use dotenv::dotenv;
use envy::from_env;


#[derive(Clone, Deserialize, Debug)]
pub struct Config {
    pub rust_log: String,
    pub server_url: String,
    pub work_dir: String,
    pub preview_width: u32,
    pub preview_height: u32,
    pub max_json_payload_size: usize,
}

lazy_static! {
    /// Global config instance serialized from environments (.env file).
    pub static ref CONFIG: Config = initialize_config();
}

/// Initialize a config.
fn initialize_config() -> Config {
    dotenv().ok();

    from_env().expect("could not load config")
}

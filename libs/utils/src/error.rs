use actix_web::{error, http::StatusCode};
use derive_more::Display;


pub type WebResult<T> = Result<T, WebError>;

/// Internal errors that are able to be converted to actix_web::error::Error.
#[derive(Debug, Display)]
pub enum WebError {
    #[display(fmt = "Internal error")]
    InternalError,
    #[display(fmt = "Payload overflow")]
    PayloadOverflow,
    #[display(fmt = "Invalid multipart/form-data request. Reason: {:?}", _0)]
    BadMultipartRequest(String),
    #[display(fmt = "Expect content-type: {}", _0)]
    InvalidContentType(&'static str),
    #[display(fmt = "Expect content-disposition")]
    NoContentDisposition,
    #[display(fmt = "Expect content-disposition: filename")]
    NoContentDispositionFilename,
    #[display(fmt = r#"Expect content-disposition: name="{}""#, _0)]
    InvalidContentDispositionName(&'static str),
    #[display(fmt = "Invalid Json: {:?}", _0)]
    JsonDeserialize(String),
    #[display(fmt = "Invalid image")]
    ImageDeserialize,
    #[display(fmt = "Invalid Base64-encoded image")]
    ImageBase64Deserialize,
    #[display(fmt = "Invalid filename")]
    InvalidFilename,
    #[display(fmt = "Invalid URL")]
    InvalidUrl,
    #[display(fmt = "Invalid remote file")]
    InvalidRemoteFile,
}

/// A ResponseError will contain a body with an error description
/// declared in Display trait for the WebError.
impl error::ResponseError for WebError {
    fn status_code(&self) -> StatusCode {
        match *self {
            WebError::InternalError => StatusCode::INTERNAL_SERVER_ERROR,
            WebError::PayloadOverflow => StatusCode::PAYLOAD_TOO_LARGE,
            _ => StatusCode::BAD_REQUEST,
        }
    }
}

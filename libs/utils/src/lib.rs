//! The `urils` crate lib includes common structures like `WebResult`,
//! global `CONFIG` instance and OpenCV FFI wrapper.

#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;

mod config;
mod error;
mod opencv;

pub use config::{Config, CONFIG};
pub use error::{WebResult, WebError};
pub use opencv::{IplImage, IsColor, Interpolation, resize};

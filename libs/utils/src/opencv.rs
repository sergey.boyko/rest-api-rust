use crate::{WebResult, WebError};
use std::ffi::CString;
use libc::{c_int, c_char};


pub enum Interpolation
{
    _CvInterNn = 0,
    CvInterLinear = 1,
    _CvInterCubic = 2,
    _CvInterArea = 3,
    _CvInterLanczos4 = 4,
}

pub enum IsColor {
    _CvLoadImageUnchanged = -1,
    _CvLoadImageGrayScale = 0,
    CvLoadImageColor = 1,
    _CvLoadImageAnyDepth = 2,
    _CvLoadImageAnyColor = 4,
}

pub struct IplImage {
    inner: *mut opencv_api::IplImage,
}

/// Resize `src` image to `dst` using width and height of the `dst` image.
pub fn resize(src: &IplImage, dst: &mut IplImage, interpolation: Interpolation) {
    unsafe {
        opencv_api::cvResize(src.inner as *const opencv_api::CvArr,
                             dst.inner as *mut opencv_api::CvArr,
                             interpolation as c_int);
    }
}

impl IplImage {
    /// Load `IplImage` using specified parameters.
    pub fn from_size(width: u32, height: u32, depth: i32, channel: i32) -> WebResult<Self> {
        unsafe {
            let size = opencv_api::CvSize::new(width, height);
            let inner =
                opencv_api::cvCreateImage(size, depth as c_int, channel as c_int);

            if inner.is_null() {
                error!("could not create an OpenCV image");
                return Err(WebError::InternalError);
            }

            Ok(Self { inner })
        }
    }

    /// Load `IplImage` from file.
    pub fn from_file(filename: String, iscolor: IsColor) -> WebResult<IplImage> {
        unsafe {
            let filename = CString::new(filename)
                .map_err(|err| {
                    error!("error on CString conversion. Error: {}", &err);
                    WebError::InternalError
                })?;
            let filename = filename.as_ptr() as *const c_char;

            let inner = opencv_api::cvLoadImage(filename, iscolor as c_int);
            if inner.is_null() {
                return Err(WebError::ImageDeserialize);
            }

            Ok(Self { inner })
        }
    }

    /// Save file from the `IplImage`.
    pub fn save(&self, filename: String) -> WebResult<()> {
        unsafe {
            let filename = CString::new(filename)
                .map_err(|err| {
                    error!("error on CString conversion. Error: {}", &err);
                    WebError::InternalError
                })?;
            let filename = filename.as_ptr() as *const c_char;

            match opencv_api::cvSaveImage(filename,
                                          self.inner as *const opencv_api::CvArr,
                                          std::ptr::null()) {
                1 => Ok(()),
                _ => {
                    error!("could not save OpenCV image");
                    Err(WebError::InternalError)
                }
            }
        }
    }

    pub fn channels_count(&self) -> i32 {
        unsafe {
            (*self.inner).n_channels as i32
        }
    }

    pub fn depth(&self) -> i32 {
        unsafe {
            (*self.inner).depth as i32
        }
    }
}

/// Implement destructor for the `IplImage`.
impl Drop for IplImage {
    fn drop(&mut self) {
        unsafe {
            opencv_api::cvReleaseImage(&mut self.inner);
        }
    }
}

/// Opencv FFI API.
mod opencv_api {
    use libc::{c_int, c_char};

    #[repr(C)]
    pub struct CvSize {
        pub width: c_int,
        pub height: c_int,
    }

    /// Declare not all fields of the original IplImage: we need only the n_channels and depth.
    #[repr(C)]
    pub struct IplImage {
        /// sizeof(IplImage)
        _n_size: c_int,
        /// version (=0)
        _id: c_int,
        /// Most of OpenCV functions support 1,2,3 or 4 channels
        pub n_channels: c_int,
        /// Ignored by OpenCV
        _alpha_channel: c_int,
        /// Pixel depth in bits: IPL_DEPTH_8U, IPL_DEPTH_8S, IPL_DEPTH_16S,
        /// IPL_DEPTH_32S, IPL_DEPTH_32F and IPL_DEPTH_64F are supported.
        pub depth: c_int,
        // Other opaque fields ...
    }

    /// Declare the CvArr as an empty structure even though the original structure has fields.
    /// TODO replace it with an extern type
    #[repr(C)]
    pub struct CvArr {
        _opaque: [u8; 0]
    }

    impl CvSize {
        pub fn new(width: u32, height: u32) -> Self {
            let width = width as c_int;
            let height = height as c_int;
            Self { width, height }
        }
    }

    #[link(name = "opencv_core")]
    extern {
        /// Creates IPL image (header and data).
        /// Note the C function was replaced into opencv_highgui library in > 3.X versions.
        pub fn cvCreateImage(size: CvSize, depth: c_int, channel: c_int) -> *mut IplImage;

        /// Releases IPL image header and data.
        pub fn cvReleaseImage(image: *mut *mut IplImage);
    }

    #[link(name = "opencv_imgproc")]
    extern {
        /// Resize `src` image and write into `dst` with default interpolation = CvInterLinear.
        pub fn cvResize(src: *const CvArr, dst: *mut CvArr, interpolation: c_int);
    }

    #[link(name = "opencv_highgui")]
    extern {
        /// Load an image from the specified file with default param: iscolor = CV_LOAD_IMAGE_COLOR.
        pub fn cvLoadImage(filename: *const c_char, iscolor: c_int) -> *mut IplImage;

        /// Save an image to the specified file with default params = 0.
        /// Returns 0 if the operation is finished successfully.
        pub fn cvSaveImage(filename: *const c_char,
                           image: *const CvArr,
                           params: *const c_int) -> c_int;
    }
}

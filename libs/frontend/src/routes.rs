use super::upload_handle;
use actix_web::web;
use actix_files as fs;


/// Declare URL routes.
pub fn routes(cfg: &mut web::ServiceConfig) {
    cfg
        // Operations with files scope
        .service(
            web::scope("files")
                .route("/upload", web::post().to(upload_handle))
        )
        .service(
            web::scope("").default_service(
                fs::Files::new("", "./static")
                    .index_file("index.html")
                    .use_last_modified(true)
            )
        );
}

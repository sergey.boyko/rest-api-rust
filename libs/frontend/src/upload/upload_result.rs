//! The module contains structures that are able to be converted to `upload` method response.

use utils::WebError;

#[derive(Serialize, Deserialize)]
pub struct UploadResult {
    pub images: Vec<ImageUploadStatus>
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum ImageUploadStatus {
    Success(ImageUploadOk),
    Failure(ImageUploadError),
}

#[derive(Serialize, Deserialize)]
pub struct ImageUploadOk {
    pub filename: Option<String>,
    pub data: String,
}

#[derive(Serialize, Deserialize)]
pub struct ImageUploadError(pub String);

impl UploadResult {
    pub fn new() -> Self {
        Self { images: Vec::new() }
    }

    pub fn push(&mut self, image_result: ImageUploadOk) {
        self.images.push(ImageUploadStatus::Success(image_result));
    }

    pub fn push_err(&mut self, err: WebError) {
        self.images.push(ImageUploadStatus::Failure(ImageUploadError::new(err)));
    }
}

impl ImageUploadOk {
    pub fn new(preview: backend::ImagePreviewBase64) -> Self {
        Self { filename: None, data: preview.into_string() }
    }

    pub fn with_filename(filename: String, preview: backend::ImagePreviewBase64) -> Self {
        Self { filename: Some(filename), data: preview.into_string() }
    }
}

impl ImageUploadError {
    pub fn new(err: WebError) -> Self {
        ImageUploadError(format!("{}", err))
    }
}


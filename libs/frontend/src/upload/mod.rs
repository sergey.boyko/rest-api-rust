mod upload;
mod upload_result;

pub use upload::{JsonImage, Base64Image, upload};
pub use upload_result::{UploadResult, ImageUploadOk, ImageUploadStatus, ImageUploadError};

use super::{UploadResult, ImageUploadOk};
use utils::{WebResult, WebError, CONFIG};

use actix_web::HttpResponse;
use actix_multipart::{Multipart, Field, MultipartError};
use futures::{StreamExt, TryStreamExt};
use bytes::BytesMut;
use mime;


/// A "/files/upload" REST method entry point.
/// The method takes multipart/form-data request that may contain one or more parts.
/// Each part is expected to has the following headers:
/// `Content-Disposition: form-data; name="images" [; filename="<filename>";]`
/// where the filename is optional parameter (used only if the `Content-Type: image/*`).
///
/// Each part is expected to has one of the following payload structures:
/// - Raw image data (filename is specified within the `Content-Disposition` of the part).
/// - Base64 encoded file.
///
/// ```json
/// {
///     "base64": {
///         "filename": String,
///         "data": String
///     }
/// }
/// ```
///
/// - URL (uploading using the specified URL).
///
/// ```json
/// {
///     "url": String
/// }
/// ```
///
/// Response is either text/plan error description (if the multipart request is invalid),
/// or the following JSON structure:
///
/// ```json
/// {
///     "images": [
///         {
///             "success": [null, { "filename": String, "data": String}],
///             "failure": [null, String]
///         }
///     ]
/// }
/// ```
/// where the `data` contains Base64 encoded preview.
pub async fn upload(mut payload: Multipart) -> WebResult<HttpResponse> {
    let mut result = UploadResult::new();

    let mut field_idx = 0;
    while let Some(item) = payload.next().await {
        let field: Field = item.map_err(|err| from_multipart_error(err))?;

        let field_result =
            if field.content_type().type_() == mime::IMAGE {
                handle_image(field).await
            } else if field.content_type() == &mime::APPLICATION_JSON {
                handle_json(field).await
            } else {
                Err(WebError::InvalidContentType("application/json | image/*"))
            };

        match field_result {
            Ok(ok) => {
                debug!("multipart/form-data [{}]: {:?} has been processed correctly",
                       field_idx,
                       &ok.filename.clone().unwrap_or("REMOTE IMAGE".to_string()));
                result.push(ok)
            }
            Err(err) => {
                debug!("multipart/form-data [{}]: ERROR {}", field_idx, &err);
                result.push_err(err)
            }
        }

        field_idx = field_idx + 1;
    }
    Ok(HttpResponse::Ok()
        .content_type("application/json")
        .json(result))
}

#[derive(Serialize, Deserialize)]
pub struct Base64Image {
    pub filename: String,
    pub data: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum JsonImage {
    Url(String),
    Base64(Base64Image),
}

/// Handle JSON that may contain either `base64` or `url` field.
async fn handle_json(json: Field) -> WebResult<ImageUploadOk> {
    match read_json(json).await? {
        JsonImage::Url(url) => backend::upload_from_url(url).await.
            // do not set the filename, because the image was uploaded via URL
            map(|image| ImageUploadOk::new(image)),
        JsonImage::Base64(json) => {
            let filename = json.filename;
            backend::upload_from_base64(backend::Base64Image::new(filename.clone(), json.data)).await
                .map(|image|
                    ImageUploadOk::with_filename(filename, image))
        }
    }
}

/// Read `JsonImage` from field data.
async fn read_json(mut json: Field) -> WebResult<JsonImage> {
    let content_disposition = json.content_disposition()
        .ok_or(WebError::NoContentDisposition)?;

    match content_disposition.get_name() {
        Some("images") => (),
        _ => return Err(WebError::InvalidContentDispositionName("images")),
    };

    let mut body = BytesMut::with_capacity(CONFIG.max_json_payload_size);
    while let Some(chunk) = json.next().await {
        let data = chunk
            .map_err(|err| from_multipart_error(err))?;
        if (body.len() + data.len()) > CONFIG.max_json_payload_size {
            return Err(WebError::PayloadOverflow);
        }

        body.extend_from_slice(&data);
    }

    serde_json::from_slice(&body)
        .map_err(|err| from_json_error(err))
}

/// Handle raw image data.
async fn handle_image(image: Field) -> WebResult<ImageUploadOk> {
    let content_disposition = image.content_disposition()
        .ok_or(WebError::NoContentDisposition)?;

    match content_disposition.get_name() {
        Some("images") => (),
        _ => return Err(WebError::InvalidContentDispositionName("images")),
    };

    let filename = content_disposition.get_filename()
        .ok_or(WebError::NoContentDispositionFilename)?;

    let fileinfo = backend::RawImageInfo::new(filename.to_string());

    let data_block = image.map_err(|err| from_multipart_error(err));

    backend::upload_from_stream(fileinfo, data_block).await
        .map(|image_preview|
            ImageUploadOk::with_filename(filename.to_string(), image_preview))
}

fn from_json_error(err: serde_json::Error) -> WebError {
    WebError::JsonDeserialize(format!("{}", err))
}

fn from_multipart_error(err: MultipartError) -> WebError {
    WebError::BadMultipartRequest(format!("{}", err))
}

//! The `frontend` crate lib includes URL routes configuration and `upload` REST method.

#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;

mod routes;
mod upload;

pub use routes::routes;

/// Note do not use the functions except in tests
pub use upload::{UploadResult, ImageUploadOk, JsonImage, ImageUploadStatus, ImageUploadError,
                 Base64Image, upload as upload_handle};

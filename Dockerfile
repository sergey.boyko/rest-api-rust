FROM ubuntu:16.04

WORKDIR /usr/local/src/web-srv
COPY . .

RUN apt update && apt install -y libopencv-dev && apt install -y cargo && apt install -y libssl-dev

# Create key.pem and self-signed cert.pem if there are no these files

RUN if { [ ! -f cert.pem ] || [ ! -f key.pem ]; }; then openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'; fi

RUN cargo build
RUN cargo test

CMD ["cargo", "run"]

use frontend::{UploadResult, ImageUploadStatus, ImageUploadError, JsonImage,
               Base64Image, upload_handle};
use utils::WebError;

use actix_web::test;
use actix_web::{web, App};
use bytes::Buf;
use futures::TryStreamExt;

const PNG_IMAGE: &'static [u8] = include_bytes!("tstimg_1.png");
const JPEG_IMAGE: &'static [u8] = include_bytes!("tstimg_2.jpeg");

const PREVIEW: &[u8] = include_bytes!("tstimg_1.preview.png");


// TODO add URL uploading tests

#[actix_rt::test]
async fn test_upload_common_ok() {
    let json = Base64Image {
        filename: "upload_base64.png".to_string(),
        data: base64::encode(PREVIEW),
    };
    let base64_payload = serde_json::to_string(&JsonImage::Base64(json)).unwrap();

    // build multipart/form-data payload with the following three fields
    let mut payload = Vec::new();
    append_multipart_field(&mut payload, Some("upload_tstimg_1.png"), "image/png", PNG_IMAGE);
    append_multipart_field(&mut payload, Some("upload_tstimg_2.jpeg"), "image/jpeg", JPEG_IMAGE);
    append_multipart_last_field(&mut payload, None, "application/json", base64_payload.as_bytes());


    // build request with the headers and payload
    let req = test::TestRequest::post()
        .header("Content-Type",
                "multipart/form-data; boundary=----5nqyPwg1EHLXBAkD")
        .header("Content-Length", payload.len())
        .set_payload(payload)
        .to_request();

    // init test service that allows to execute the `upload` handler
    let mut app = test::init_service(
        App::new().route("/", web::post().to(upload_handle)))
        .await;
    let mut resp = test::call_service(&mut app, req).await;

    // body as bytes
    let bytes = test::load_stream(resp.take_body().into_stream()).await.unwrap();
    // body as json
    let result: UploadResult = serde_json::from_slice(bytes.bytes()).unwrap();

    assert_eq!(result.images.len(), 3);
    for img in result.images.into_iter() {
        let img = match img {
            ImageUploadStatus::Success(x) => x,
            // expect success in processing all of the images
            _ => panic!("expect Ok"),
        };
        // TODO make the filename check stricter
        match img.filename.unwrap().as_str() {
            "upload_tstimg_1.png" | "upload_tstimg_2.jpeg" | "upload_base64.png" => (),
            _ => panic!("unexpected image"),
        }
    }
}

#[actix_rt::test]
async fn test_upload_invalid_img() {
    let mut payload = Vec::new();
    append_multipart_last_field(&mut payload,
                                Some("upload_invalid_image.png"),
                                "image/png",
                                // Invalid PNG image data
                                b"1234567");

    test_upload_invalid_img_impl(payload, WebError::ImageDeserialize).await;
}

#[actix_rt::test]
async fn test_upload_invalid_base64_img() {
    let base64_payload = Base64Image {
        filename: "upload_invalid_base64.png".to_string(),
        // invalid Base64 encoded image data
        data: "1234%67".to_string(),
    };
    let base64_payload = serde_json::to_string(&JsonImage::Base64(base64_payload)).unwrap();

    let mut payload = Vec::new();
    append_multipart_last_field(&mut payload, None, "application/json", base64_payload.as_bytes());

    test_upload_invalid_img_impl(payload, WebError::ImageBase64Deserialize).await;
}

#[actix_rt::test]
async fn test_upload_invalid_content_type() {
    let mut payload = Vec::new();
    // unexpected content type "text/plan"
    append_multipart_last_field(&mut payload, None, "text/plan", b"");

    test_upload_invalid_img_impl(
        payload, WebError::InvalidContentType("application/json | image/*")).await;
}

#[actix_rt::test]
async fn test_upload_no_filename() {
    let mut payload = Vec::new();
    // expect filename
    append_multipart_last_field(&mut payload, None, "image/png", b"");

    test_upload_invalid_img_impl(payload, WebError::NoContentDispositionFilename).await;
}

#[actix_rt::test]
async fn test_upload_invalid_json() {
    let invalid_json = "{}".to_string();

    let mut payload = Vec::new();
    append_multipart_last_field(&mut payload, None, "application/json", invalid_json.as_bytes());

    test_upload_invalid_img_impl(
        payload, WebError::JsonDeserialize("expected value at line 1 column 2".to_string())).await;
}

async fn test_upload_invalid_img_impl(payload: Vec<u8>, expect_error: WebError) {
    let req = test::TestRequest::post()
        .header("Content-Type",
                "multipart/form-data; boundary=----5nqyPwg1EHLXBAkD")
        .header("Content-Length", payload.len())
        .set_payload(payload)
        .to_request();


    let mut app = test::init_service(
        App::new().route("/", web::post().to(upload_handle)))
        .await;
    let mut resp = test::call_service(&mut app, req).await;

    // body as bytes
    let bytes = test::load_stream(resp.take_body().into_stream()).await.unwrap();
    // body as json
    let result: UploadResult = serde_json::from_slice(bytes.bytes()).unwrap();

    assert_eq!(result.images.len(), 1);
    let img = result.images.into_iter().next().unwrap();

    let ImageUploadError(err) = match img {
        ImageUploadStatus::Failure(x) => x,
        _ => panic!("expect Err"),
    };

    assert_eq!(err, format!("{}", expect_error));
}

fn append_multipart_field(payload: &mut Vec<u8>, filename: Option<&str>, content_type: &str, data: &[u8]) {
    let content_disposition =
        "Content-Disposition: form-data; name=\"images\"";
    let mut content_disposition = match filename {
        Some(x) =>
            format!("{}; filename={:?}\r\n", content_disposition, x).into_bytes(),
        _ => format!("{}\r\n", content_disposition).into_bytes(),
    };
    let mut content_type =
        format!("Content-Type: {}\r\n\r\n", content_type).into_bytes();

    payload.extend_from_slice(b"\r\n------5nqyPwg1EHLXBAkD\r\n");
    payload.append(&mut content_disposition);
    payload.append(&mut content_type);
    payload.extend_from_slice(data);
}

fn append_multipart_last_field(payload: &mut Vec<u8>, filename: Option<&str>, content_type: &str, data: &[u8]) {
    append_multipart_field(payload, filename, content_type, data);
    payload.extend_from_slice(b"\r\n------5nqyPwg1EHLXBAkD--\r\n");
}

use backend::{_gen_preview_filename, check_file_extension, check_filename, _find_filename,
              find_filename_from_url};

#[test]
fn test_gen_preview() {
    let test_equal = |original: &str, expect: &str| {
        assert_eq!(_gen_preview_filename(&original.to_string()).unwrap(),
                   expect.to_string());
    };

    test_equal("test", "test.preview.png");
    test_equal("test.jpg", "test.preview.png");
    test_equal(".test.jpg", ".test.preview.png");
    test_equal("test.jpg.png", "test.jpg.preview.png");

    assert!(_gen_preview_filename(&"".to_string()).is_err());
}

#[test]
pub fn test_check_file_extension() {
    assert!(check_file_extension("test.png").is_ok());
    assert!(check_file_extension("test.").is_err());
    assert!(check_file_extension("test").is_err());
    assert!(check_file_extension("").is_err());
}

#[test]
fn test_find_filename() {
    assert_eq!(_find_filename(r#"form-data; filename="image.jpg" name="image";"#).unwrap(),
               "image.jpg".to_string());
    assert_eq!(_find_filename(r#"form-data; name="image"; filename="FШ少_-0""#).unwrap(),
               "FШ少_-0".to_string());
    // incomplete filename
    assert!(_find_filename(r#"form-data; name="image"; filename="image.jpg"#).is_err());
    // filenam instead of filename
    assert!(_find_filename(r#"form-data; name="image"; filenam="image.jpg""#).is_err());
    // filenam instead of filename
    assert!(_find_filename("").is_err());
}

#[test]
fn test_check_filename() {
    assert!(check_filename(&"test.jpg".to_string()).is_ok());
    assert!(check_filename(&"/tmp/test.jpg".to_string()).is_err());
    assert!(check_filename(&r#"C:\tmp\test.jpg"#.to_string()).is_err());
}

#[test]
fn test_find_filename_from_url() {
    assert_eq!("zar.jpg".to_string(),
               find_filename_from_url(&"https://my.com/foo/bar/zar.jpg".to_string()).unwrap());

    // no filename
    assert!(find_filename_from_url(&"https://my.com/foo/bar/".to_string()).is_err());
    // no extension
    assert!(find_filename_from_url(&"https://my.com/foo/bar".to_string()).is_err());
    // no filename
    assert!(find_filename_from_url(&"https://my.com".to_string()).is_err());
}

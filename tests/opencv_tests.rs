use std::io::prelude::*;
use std::fs::File;
use backend::_create_png_base64_preview;

const PREVIEW_1: &[u8] = include_bytes!("tstimg_1.preview.png");
const PREVIEW_2: &[u8] = include_bytes!("tstimg_2.preview.png");


/// Write the file data to the build directory.
pub fn write_file_to_build_dir(filename: &str, data: &[u8]) -> Result<(), ()> {
    let mut buffer = File::create(filename).map_err(|_| ())?;
    buffer.write_all(data).map_err(|_| ())
}

#[tokio::test]
async fn test_create_preview_1() {
    let preview_data =
        _create_png_base64_preview("tests/tstimg_1.png".to_string(), 100, 100).await.unwrap();
    assert_eq!(preview_data.into_string(), base64::encode(PREVIEW_1));
}

#[tokio::test]
async fn test_create_preview_2() {
    let preview_data =
        _create_png_base64_preview("tests/tstimg_2.jpeg".to_string(), 100, 100).await.unwrap();
    assert_eq!(preview_data.into_string(), base64::encode(PREVIEW_2));
}
